#HT Software Engineering

##Environment

1. Ubuntu 18.04 || Mint 19.1 (Optional)
2. Java 8
3. PostgreSQL
4. Intellij IDEA Ultimate (Get student license via mail.iitu.kz)
5. Git (git bash in case of windows)


##To read

###Java Core

1. Syntax, Access modifiers, Autoboxing and etc
2. OOP (Inheritance, Encapsulation, Polymorphism
3. Collection API (ArrayList, LinkedList, HashMap, Set and etc)
4. SOLID

###SQL

1. DDL (CREATE, ALTER, DROP, RENAME and etc)
2. DML (SELECT, INSERT, UPDATE, DELETE and etc)
3. Little bit of DCL (GRANT, REVOKE) and TCL (What is transaction)

####__I will not provide any materials ~~because I'm an asshole~~, just google those keywords and read__

####__I recommend to read articles and documentation, rather than books__

####__Googling skill is the most crucial for coding__