package kz.ht.se.repostiry;

import kz.ht.se.model.Todo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TodoRepository extends CrudRepository<Todo, Long> {
    Optional<Todo> findById(Long id);

    List<Todo> findAll();

    Optional<Todo> findByTitle(String title);
}
