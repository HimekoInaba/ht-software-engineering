package kz.ht.se.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Getter
@Setter
@AllArgsConstructor
@Entity
@NoArgsConstructor
public class Todo {
    @Id
    @SequenceGenerator(name = "todo_id_seq", initialValue = 100)
    private Long id;

    private String title;
    private Boolean isDone;
    private Boolean isActive;
}
