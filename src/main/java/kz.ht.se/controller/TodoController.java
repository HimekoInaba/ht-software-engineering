package kz.ht.se.controller;

import kz.ht.se.model.Todo;
import kz.ht.se.repostiry.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class TodoController {

     @Autowired
     private TodoRepository todoRepository;

    //GET, POST
    @GetMapping("/{id}")
    public Todo getById(@PathVariable Long id) {
        Optional<Todo> todo = todoRepository.findById(id);
        if (todo.isPresent()) {
            return todo.get();
        }
        return null;
    }


    @PostMapping("/create")
    public Todo create(@RequestBody Todo todo) {
        todoRepository.save(todo);
        return todo;
    }
}
